angular.module('ibeacon.controllers', ['ngCordovaBeacon'])

  .controller('AppCtrl', function($scope) {
    $scope.resetDevice = function () {
      window.localStorage.clear();
    }
  })

  .controller('ibeaconCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaBeacon, $ionicModal, socket) {
    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {

      console.log("console test");
      $scope.beacons = {};
      $scope.device = {};

      //Modal to give the device a name
      $ionicModal.fromTemplateUrl('templates/name-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;

        //check if a device name is already given and set the devicename, otherwise set the devicename
        if ((window.localStorage['devicename'] != undefined) && !(window.localStorage['devicename'] == "undefined")) {
          $scope.devicename = window.localStorage['devicename'];
          $scope.device.name = window.localStorage['devicename'];
        } else {
          $scope.modal.show();
        }
      });

      $scope.openModal = function () {
        $scope.modal.show();
      };

      $scope.closeModal = function () {
        window.localStorage['devicename'] = $scope.device.name;
        $scope.devicename = $scope.device.name;
        $scope.modal.hide();
      };

      $ionicPlatform.ready(function () {
        //iOS background
        cordova.plugins.backgroundMode.enable();
        //Android autostart
        cordova.plugins.autoStart.enable();
        //location access
        $cordovaBeacon.requestWhenInUseAuthorization();

        $rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function (event, pluginResult) {
          if ($scope.devicename) {
            var uniqueBeaconKey;
            var newBeacons = [];
            var updateBeacons = [];
            for (var i = 0; i < pluginResult.beacons.length; i++) {
              uniqueBeaconKey = pluginResult.beacons[i].uuid + ":" + pluginResult.beacons[i].major + ":" + pluginResult.beacons[i].minor;
              //check if detected beacon is in list
              if (!(uniqueBeaconKey in $scope.beacons)) {
                //new beacon detected
                newBeacons.push(uniqueBeaconKey);
                socket.emit('enter', $scope.devicename, pluginResult.beacons[i]);
              } else {
                //beacon already in the list and gets updated
                updateBeacons.push(uniqueBeaconKey);
              }
              $scope.beacons[uniqueBeaconKey] = pluginResult.beacons[i];
            }

            //detect beacons in the list that are not detected anymore and delete them
            for (var i in $scope.beacons) {
              if (updateBeacons.indexOf(i) == -1 && newBeacons.indexOf(i) == -1) {
                delete $scope.beacons[i];
                socket.emit('left', $scope.devicename, $scope.beacons[i]);
              }
            }

            $scope.$apply();
            socket.emit('beacons', $scope.devicename, $scope.beacons);
          }
        });
        $cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("lnu", "7003FC98-1223-5961-BE4B-EECC523981E5"));
      });
    }
  });
