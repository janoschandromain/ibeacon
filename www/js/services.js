angular.module('ibeacon.services', [])
  .factory('socket', function (socketFactory) {
    var myIoSocket = io.connect('http://celtest1.lnu.se:3000');

    mySocket = socketFactory({
      ioSocket: myIoSocket
    });

    return mySocket;
  })
;
